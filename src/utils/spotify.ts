import SpotifyWebApi from 'spotify-web-api-node'
import { HttpStatusCode } from '../lib/enum/HttpStatusCode'

interface Response<T> {
  body: T
  headers: Record<string, string>
  statusCode: HttpStatusCode
}

interface BaseTokenResponse {
  access_token: string
  expires_in: number
  refresh_token?: string
  scope: string
  token_type: string
}

interface AuthorizationCodeGrantResponse extends BaseTokenResponse {
  refresh_token: string
}

interface RefreshAccessTokenResponse extends BaseTokenResponse {}

interface ErrorResponse {
  error: {
    status: HttpStatusCode
    message: string
    reason: string
  }
}

// TODO: use gitlab secrets
const baseSpotifyAuth = {
  clientId: 'a2a88c4618324942859ce3e1f888b938',
  clientSecret: 'bfce3e5d96074c21ac4db8b4991c2f37',
}

export const authorizationCodeGrantAsync = async (
  code: string,
  redirectUri: string
): Promise<Response<AuthorizationCodeGrantResponse>> => {
  const spotifyApi = new SpotifyWebApi({
    ...baseSpotifyAuth,
    redirectUri,
  })
  try {
    const token = await spotifyApi.authorizationCodeGrant(code)

    return token
  } catch (error: any) {
    console.error(error)
  }
}

export const refreshAccessTokenAsync = async (
  refreshToken: string,
  redirectUri: string
): Promise<Response<RefreshAccessTokenResponse>> => {
  const spotifyApi = new SpotifyWebApi({
    ...baseSpotifyAuth,
    redirectUri,
    refreshToken,
  })

  try {
    const token = await spotifyApi.refreshAccessToken()

    return token
  } catch (error: any) {
    console.error(error)
  }
}

export const createPlaylistAsync = async (
  token: string,
  playlistId?: string
): Promise<string> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  let tracks: string[] = []

  if (playlistId) {
    const trackObjects = await getPlaylistTracksAsync(token, playlistId)
    tracks = trackObjects.map(track => track.uri)
  }

  const playlist = await spotifyApi.createPlaylist('🟣🔴🟢🔵🟠🟡', {
    public: true,
    description: 'Playlist created with https://spotify-x.herokuapp.com/',
  })

  if (tracks.length > 0) {
    await addTracksToPlaylistAsync(token, playlist.body.id, tracks)
  }

  await playAsync(token, playlist.body.id)

  return playlist.body.id
}

export const addTracksToPlaylistAsync = async (
  token: string,
  playlistId: string,
  trackUris: string[]
): Promise<number> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  // We can only add 100 tracks at a time due to spotify constraints
  const spotifyMaxTracksPerRequest = 100

  let tracksAdded = 0

  while (tracksAdded < trackUris.length) {
    await spotifyApi.addTracksToPlaylist(
      playlistId,
      trackUris.slice(tracksAdded, tracksAdded + spotifyMaxTracksPerRequest)
    )
    tracksAdded += spotifyMaxTracksPerRequest
  }

  return tracksAdded
}

export const getUserDataAsync = async (
  token: string
): Promise<SpotifyApi.CurrentUsersProfileResponse> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  const userData = await spotifyApi.getMe()
  return userData.body
}

// TODO: fix NO_ACTIVE_DEVICE error
export const getCurrentlyPlayingAsync = async (
  token: string,
  playlistId: string
): Promise<{
  tracks: SpotifyApi.TrackObjectFull[]
  progress_ms: number
}> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  try {
    const tracks = await getPlaylistTracksAsync(token, playlistId)
    const currentlyPlaying = await getMyCurrentPlaybackStateAsync(token)

    const isPlaylist = currentlyPlaying?.context?.type === 'playlist'
    const isCurrentPlaylist = currentlyPlaying?.context?.uri?.includes(playlistId)

    if (!isPlaylist || !isCurrentPlaylist) return

    const trackIndex = await poorMansQueueIndexGuessAsync(
      token,
      tracks,
      currentlyPlaying.item
    )

    return {
      tracks: tracks.slice(trackIndex, tracks.length),
      progress_ms: currentlyPlaying.progress_ms,
    }
  } catch (error) {
    const response = error as Response<ErrorResponse>
    console.log(error)
    if (response.statusCode === HttpStatusCode.NOT_FOUND) {
      if (response.body.error.reason === 'NO_ACTIVE_DEVICE') {
        console.log('getMyCurrentPlaybackStateAsync', response.body.error)
      }
    }
  }
}

export const playAsync = async (
  token: string,
  playlistId: string,
  deviceId?: string
): Promise<void> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  try {
    await setShuffleAsync(token, false)
    await spotifyApi.play({
      offset: {
        position: 0,
      },
      device_id: deviceId,
      context_uri: `spotify:playlist:${playlistId}`,
    })
  } catch (error) {
    const response = error as Response<ErrorResponse>

    // Lets try our best to play some music for the user
    if (response.body.error.reason === 'NO_ACTIVE_DEVICE') {
      const devices = await getMyDevicesAsync(token)
      if (devices.length > 0) {
        await transferMyPlayback(
          token,
          devices.map(device => device.id)
        )
        await playAsync(token, playlistId, devices[0].id)
      }
    }
  }
}

const transferMyPlayback = async (
  token: string,
  deviceIds: string[]
): Promise<void> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  // Spotify API only supports a single device at a time
  await spotifyApi.transferMyPlayback(deviceIds.slice(0, 1))
}

const getMyDevicesAsync = async (token: string): Promise<SpotifyApi.UserDevice[]> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  const devices = await spotifyApi.getMyDevices()

  return devices.body.devices
}

const poorMansQueueIndexGuessAsync = async (
  token: string,
  tracks: SpotifyApi.TrackObjectFull[],
  currentlyPlaying: SpotifyApi.TrackObjectFull | SpotifyApi.EpisodeObject
): Promise<number> => {
  const trackIds = tracks.map(track => track.id)
  const foundTracks = trackIds.filter(track => track === currentlyPlaying.id)

  if (foundTracks.length === 1) {
    // Nice, added only once
    return trackIds.indexOf(foundTracks[0])
  }

  // We're not sure what the index is, let's check the previous tracks
  const recentlyPlayedTracks = await getMyRecentlyPlayedTracksAsync(token)
  const recentlyPlayedTrackIds = recentlyPlayedTracks
    .map(track => track.track.id)
    .reverse()
  const trackIndexProbability: { index: number; probability: number }[] = []

  // Let's reverse the recent played tracks
  for (const foundTrack in foundTracks) {
    const index = trackIds.indexOf(foundTrack)

    let probability = 0
    for (let i = 0; i <= recentlyPlayedTrackIds.length; i++) {
      // If the track is in the recently played tracks, it's a good guess
      if (trackIds[index - i - 1] === recentlyPlayedTrackIds[i]) {
        probability++
      }
    }

    trackIndexProbability.push({ index, probability })
  }

  const sorted = trackIndexProbability.sort((a, b) => b.probability - a.probability)

  // Return the best guess we have
  return sorted[0].index
}

const setShuffleAsync = async (token: string, shuffle: boolean): Promise<void> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  await spotifyApi.setShuffle(shuffle)
}

const getMyCurrentPlaybackStateAsync = async (
  token: string
): Promise<SpotifyApi.CurrentlyPlayingResponse> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  try {
    const response = await spotifyApi.getMyCurrentPlaybackState()

    return response.body
  } catch (error: any) {
    console.error('No current playback state', error)
    // throw 'No current playback state'
  }
}

const getMyRecentlyPlayedTracksAsync = async (
  token: string
): Promise<SpotifyApi.PlayHistoryObject[]> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)

  const response = await spotifyApi.getMyRecentlyPlayedTracks()

  return response.body.items
}

const getPlaylistTracksAsync = async (
  token: string,
  playlistId: string
): Promise<SpotifyApi.TrackObjectFull[]> => {
  const spotifyApi = new SpotifyWebApi(baseSpotifyAuth)
  spotifyApi.setAccessToken(token)
  try {
    let total = 0
    let received: SpotifyApi.PlaylistTrackObject[] = []

    let response = await spotifyApi.getPlaylistTracks(playlistId)

    total = response.body.total
    received = received.concat(response.body.items)

    while (received.length < total) {
      response = await spotifyApi.getPlaylistTracks(playlistId, {
        offset: received.length,
      })

      received = received.concat(response.body.items)
    }

    const tracks = received.map(item => item.track as SpotifyApi.TrackObjectFull)

    return tracks
  } catch (error) {
    console.error(error)
  }
}
