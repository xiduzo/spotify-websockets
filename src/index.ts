import express from 'express'
import actuator from 'express-actuator'
import { createServer } from 'http'
import { createSocketHub } from './sockets'

const app = express()
app.use(actuator())

const server = createServer(app)
createSocketHub(server)

const port = process.env.NODE_PORT ?? process.env.PORT ?? 8000

server.listen(port, async () => {
  console.log('Server running', server.address())
})
