import { DomainEvent } from '../lib/classes/DomainEvent'

export interface SetPlaylistData {
  playlistId?: string
}

export class SetPlaylist extends DomainEvent<SetPlaylistData> {
  get playlistId(): string | undefined {
    return this.data.playlistId
  }

  constructor(data: SetPlaylistData) {
    super(data)
  }

  protected FromJson(json: any): SetPlaylist {
    return JSON.parse(json) as SetPlaylist
  }
}
