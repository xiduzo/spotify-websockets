import { DomainEvent } from '../lib/classes/DomainEvent'
import { Vote } from '../lib/interface/Vote'

export interface AddVoteData {
  vote: Vote
}

export class AddVote extends DomainEvent<AddVoteData> {
  get vote() {
    return this.data.vote
  }

  constructor(data: AddVoteData) {
    super(data)
  }

  protected FromJson(json: any): AddVote {
    return JSON.parse(json) as AddVote
  }
}
