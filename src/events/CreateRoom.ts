import { DomainEvent } from '../lib/classes/DomainEvent'

export interface CreateRoomData {
  pin: string
  hostToken: string
  hostId: string
}

export class CreateRoom extends DomainEvent<CreateRoomData> {
  get pin() {
    return this.data.pin
  }

  get hostToken() {
    return this.data.hostToken
  }

  get hostId() {
    return this.data
  }

  constructor(data: CreateRoomData) {
    super(data)
  }

  protected FromJson(json: any): CreateRoom {
    return JSON.parse(json) as CreateRoom
  }
}
