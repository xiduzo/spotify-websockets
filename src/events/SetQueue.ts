import { DomainEvent } from '../lib/classes/DomainEvent'

export interface SetQueueData {
  trackUris: string[]
}

export class SetQueue extends DomainEvent<SetQueueData> {
  get queue() {
    return this.data.trackUris
  }

  constructor(data: SetQueueData) {
    super(data)
  }

  protected FromJson(json: any): SetQueue {
    return JSON.parse(json) as SetQueue
  }
}
