import { DomainEvent } from '../lib/classes/DomainEvent'
import { InMemStore } from '../lib/classes/InMemStore'

export class EventStore extends InMemStore<string, DomainEvent<unknown>[]> {
  constructor() {
    super()
  }

  public async SaveEventAsync(event: DomainEvent<unknown>): Promise<void> {
    try {
      const events = await super.GetByIdAsync(event.aggregateId)
      await super.SetByIdAsync(event.aggregateId, [...events, event])
    } catch (error: unknown) {
      await super.SetByIdAsync(event.aggregateId, [event])
    } finally {
      Promise.resolve()
    }
  }

  public async GetEventsAsync(aggregateId: string): Promise<DomainEvent<unknown>[]> {
    const events = await super.GetByIdAsync(aggregateId)
    return Promise.resolve(events || [])
  }
}

export const EventStoreFactory = (function () {
  let instance: EventStore
  return function () {
    if (!instance) {
      instance = new EventStore()
    }
    return instance
  }
})()

export const eventStore = EventStoreFactory()
Object.freeze(eventStore)
