import { v4 as uuid } from 'uuid'
import { AddVote, AddVoteData } from '../events/AddVote'
import { CreateRoom, CreateRoomData } from '../events/CreateRoom'
import { SetPlaylist, SetPlaylistData } from '../events/SetPlaylist'
import { SetQueue, SetQueueData } from '../events/SetQueue'
import { Aggregate } from '../lib/classes/Aggregate'
import { DomainEvent } from '../lib/classes/DomainEvent'
import { Vote } from '../lib/interface/Vote'
import {
  addTracksToPlaylistAsync,
  getCurrentlyPlayingAsync,
  playAsync,
} from '../utils/spotify'
import { eventStore } from './EventStore'

export class Room extends Aggregate {
  private _pin: string
  private _hostToken: string
  private _hostId: string
  private _playlistId?: string
  private _votes: Vote[] = []
  private _queue: string[] = []

  get pin() {
    return this._pin
  }

  get votes() {
    return this._votes
  }

  get playlistId() {
    return this._playlistId
  }

  get queue() {
    return this._queue
  }

  constructor(events?: DomainEvent<unknown>[]) {
    super(events ? events[0]?.aggregateId ?? uuid() : uuid())

    if (events) super.ApplyEvents(events)
    super.AddApplyMethod(this.ApplyEvent)
  }

  public AsDto(): unknown {
    return {
      id: this.id,
      pin: this.pin,
      votes: this.votes,
      queue: this.queue,
    }
  }

  //#region Events
  public CreateRoom(pin: string, hostId: string, hostToken: string): void {
    const event = new CreateRoom({ pin, hostId, hostToken })
    this.RaiseEvent(event)
  }

  public SetPlaylistId(playlistId?: string): void {
    const event = new SetPlaylist({ playlistId })
    this.RaiseEvent(event)
  }

  public AddVote(vote: Vote): void {
    const event = new AddVote({ vote })
    this.RaiseEvent(event)
  }

  public async AddTracks(trackUris: string[]): Promise<number> {
    if (!(await this.CanInteractWithPlaylist())) return

    const numbersAdded = await addTracksToPlaylistAsync(
      this._hostToken,
      this._playlistId,
      trackUris
    )

    await this.SetCurrentlyPlaying()

    return Promise.resolve(numbersAdded)
  }

  public async Play(): Promise<void> {
    if (!(await this.CanInteractWithPlaylist())) return

    await playAsync(this._hostToken, this._playlistId)

    await this.SetCurrentlyPlaying()

    await this.SaveRoom()
  }

  public async SetCurrentlyPlaying(): Promise<{
    trackUris: string[]
    progress_ms?: number
  }> {
    if (!(await this.CanInteractWithPlaylist())) return

    try {
      const currentlyPlaying = await getCurrentlyPlayingAsync(
        this._hostToken,
        this._playlistId
      )

      const { tracks, progress_ms } = currentlyPlaying

      // Start from beginning?
      if (tracks.length === 0)
        return {
          trackUris: [],
          progress_ms: 0,
        }

      const trackUris = tracks.map(track => track.uri)

      // Poor mans check if queue has changed
      if (
        this._queue.length === trackUris.length &&
        this._queue[0] === trackUris[0] &&
        this._queue[this._queue.length - 1] === trackUris[trackUris.length - 1]
      )
        return {
          trackUris: [],
          progress_ms,
        }

      this.RaiseEvent(new SetQueue({ trackUris }))
      return {
        trackUris,
        progress_ms,
      }
    } catch {
      console.log('Unable to get queue')
    }
  }

  //#region Save and get room
  public async SaveRoom(): Promise<void> {
    await super.SaveEvents(async event => {
      await eventStore.SaveEventAsync(event)
    })
    Promise.resolve()
  }

  public static async GetRoom(id: string): Promise<Room> {
    try {
      const events = await eventStore.GetByIdAsync(id)

      var room = new Room(events)
      return Promise.resolve(room)
    } catch (error: unknown) {
      return Promise.reject('Unable to get room')
    }
  }

  //#region Aggregate overrides
  protected ApplyEvent(event: DomainEvent<unknown>): void {
    switch (event.type) {
      case CreateRoom.name:
        this.ApplyCreateRoom(event as DomainEvent<CreateRoomData>)
        break
      case AddVote.name:
        this.ApplyAddVote(event as DomainEvent<AddVoteData>)
        break
      case SetPlaylist.name:
        this.ApplySetPlaylist(event as DomainEvent<SetPlaylistData>)
        break
      case SetQueue.name:
        this.ApplySetQueue(event as DomainEvent<SetQueueData>)
        break
    }
  }
  //#endregion

  //#region ApplyEvents
  private ApplyCreateRoom(event: DomainEvent<CreateRoomData>): void {
    this._pin = event.data.pin
    this._hostToken = event.data.hostToken
    this._hostId = event.data.hostId
  }

  private ApplyAddVote(event: DomainEvent<AddVoteData>): void {
    this._votes.push(event.data.vote)
  }

  private async ApplySetPlaylist(event: DomainEvent<SetPlaylistData>): Promise<void> {
    this._playlistId = event.data.playlistId
  }

  private async ApplySetQueue(event: DomainEvent<SetQueueData>): Promise<void> {
    this._queue = event.data.trackUris
  }

  private async CanInteractWithPlaylist(): Promise<boolean> {
    if (!this._playlistId) return false
    if (!this._hostToken) return false

    return true
  }
  //#endregion
}
