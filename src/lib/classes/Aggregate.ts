import { v4 as uuid } from 'uuid'

import { DomainEvent } from './DomainEvent'

type ApplyMethod = (event: DomainEvent<unknown>) => void

export class Aggregate {
  private _id = uuid()
  private _version = 0
  private _eventsToSave: DomainEvent<unknown>[] = []
  private _applyEvent: ApplyMethod = () => {
    throw new Error('Not implemented')
  }

  get id() {
    return this._id
  }

  constructor(id?: string) {
    this._id = id
  }

  public AsDto(): unknown {
    return {
      id: this._id,
      version: this._version,
    }
  }

  protected ApplyEvents(events: DomainEvent<unknown>[]): void {
    if (!events) return
    if (events.length <= 0) return

    this._id = events[0].aggregateId
    events.map(event => {
      this.ApplyEvent(event)
      this._version = event.aggregateVersion
    })
  }

  protected AddApplyMethod(applyEvent: ApplyMethod): void {
    this._applyEvent = applyEvent
  }

  protected RaiseEvent(event: DomainEvent<unknown>): DomainEvent<unknown> {
    event = event.WithAggregate(this._id, this._version)
    this._eventsToSave.push(event)
    try {
      this.ApplyEvent(event)
      this._version++
    } catch (error: unknown) {
      console.error('Error applying event', error)
    }
    return event
  }

  protected ApplyEvent(event: DomainEvent<unknown>): void {
    if (event.aggregateVersion !== this._version) {
      throw Error(
        `Aggregate version mismatch. Event version: ${event.aggregateVersion}, Aggregate version: ${this._version}`
      )
    }

    this._version = event.aggregateVersion
    this._applyEvent(event)
  }

  protected async SaveEvents(
    saveMethod: (event: DomainEvent<unknown>) => Promise<void>
  ): Promise<void> {
    for (const event of this._eventsToSave) {
      await saveMethod(event)
      this.EventSaved(event)
    }

    return Promise.resolve()
  }

  private EventSaved(event: DomainEvent<unknown>): void {
    this._eventsToSave = this._eventsToSave.filter(_event => _event.id !== event.id)
  }
}
