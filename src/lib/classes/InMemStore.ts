export class InMemStore<Key, Value> {
  private _items = new Map<Key, Value>()

  constructor() {}

  public SetByIdAsync(key: Key, value: Value): Promise<Value> {
    this._items.set(key, value)
    return Promise.resolve(value)
  }

  public RemoveByIdAsync(key: Key): Promise<boolean> {
    const _item = this._items.get(key)

    if (!_item) return Promise.resolve(true)

    this._items.delete(key)
    return Promise.resolve(true)
  }

  public GetByIdAsync(key: Key): Promise<Value> {
    const _item = this._items.get(key)

    if (!_item) return Promise.reject(key)

    return Promise.resolve(_item)
  }

  public GetIdsAsync(): Promise<Key[]> {
    const _keys = Array.from(this._items.keys())

    if (_keys.length === 0) return Promise.resolve([] as Key[])

    return Promise.resolve(_keys)
  }
}
