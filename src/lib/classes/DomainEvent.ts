import { v4 as uuid } from 'uuid'

export class DomainEvent<T> {
  private _id: string
  private _aggregateId: string
  private _type: string
  private _aggregateVersion: number
  private _data: T

  get id() {
    return this._id
  }

  get aggregateId() {
    return this._aggregateId
  }

  get aggregateVersion() {
    return this._aggregateVersion
  }

  get data() {
    return this._data
  }

  get type() {
    return this._type
  }

  constructor(data?: T) {
    this._id = uuid()
    this._type = this.constructor.name

    if (data) this._data = data
  }

  public WithAggregate(aggregateId: string, aggregateVersion: number) {
    this._aggregateId = aggregateId
    this._aggregateVersion = aggregateVersion

    return this
  }

  protected FromJson(json: any): DomainEvent<T> {
    return JSON.parse(json) as DomainEvent<T>
  }
}
