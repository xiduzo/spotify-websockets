import { Sign } from '../enum/Sign'

export interface Vote {
  userId: string
  trackRequestId: string
  sign: Sign
}
