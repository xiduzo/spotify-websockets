import { Server } from 'http'
import { Server as ServerCreator, Socket } from 'socket.io'
import { Room } from './eventSourcing/Room'
import { InMemStore } from './lib/classes/InMemStore'
import { generateRoomPin } from './utils/pin'
import {
  authorizationCodeGrantAsync,
  createPlaylistAsync,
  getUserDataAsync,
  refreshAccessTokenAsync,
} from './utils/spotify'

export const createSocketHub = (server: Server) => {
  const users = new InMemStore<
    string,
    { token?: string; userId?: string; roomId?: string }
  >()
  const rooms = new InMemStore<string, { id: string; host: string }>()
  const timers = new InMemStore<string, NodeJS.Timeout>()

  const io = new ServerCreator(server, {
    cors: {
      origin: ['https://spotify-x.herokuapp.com', 'http:localhost:3000'],
    },
  })

  const updateUser = (
    socketId: string,
    data: { token?: string; userId?: string; roomId?: string }
  ) => {
    try {
      const user = users.GetByIdAsync(socketId)
      users.SetByIdAsync(socketId, { ...user, ...data })
    } catch {
      users.SetByIdAsync(socketId, data)
    }
  }

  io.on('connection', (socket: Socket) => {
    //#region Room management
    socket.on('room:join:request', async (pin: string) => {
      try {
        const { id } = await rooms.GetByIdAsync(pin)
        const room = await Room.GetRoom(id)
        // TODO: update room queue on number change
        room.SetCurrentlyPlaying()
        room.SaveRoom()
        socket.join(room.id)
        updateUser(socket.id, { roomId: room.id })
        socket.emit('room:join:success', room.AsDto())
      } catch (error) {
        socket.emit('room:join:not-found', pin)
      }
    })

    socket.on('room:creation:request', async (playlistId?: string) => {
      try {
        const { token, userId } = await users.GetByIdAsync(socket.id)

        if (!token) {
          socket.emit('room:creation:error', new Error('No spotify token found'))
          return
        }
        if (!userId) {
          socket.emit('room:creation:error', new Error('Spotify username not found'))
          return
        }

        const pin = generateRoomPin(await rooms.GetIdsAsync())
        const room = new Room()

        room.CreateRoom(pin, userId, token)
        await room.SaveRoom()

        // Save the room for other sockets to join by pin code
        rooms.SetByIdAsync(room.pin, {
          id: room.id,
          host: userId,
        })
        updateUser(socket.id, { roomId: room.id })

        socket.emit('room:creation:success', room.AsDto())

        // const newPlaylist = await createPlaylistAsync(token, playlistId)
        room.SetPlaylistId(playlistId)

        await room.Play()
        await room.SaveRoom()

        const sendQueue = async () => {
          const playbackState = await room.SetCurrentlyPlaying()

          console.log('interval', playbackState)
          io.to(room.id).emit('room:track:progress', playbackState.progress_ms)

          if (playbackState.trackUris.length <= 0) return

          // TODO: only send queue when queue changes
          io.to(room.id).emit('room:queue', room.queue)
          await room.SaveRoom()
        }

        try {
          const currentInterval = await timers.GetByIdAsync(userId)
          if (currentInterval) clearInterval(currentInterval)
        } catch {
          console.log('no current interval found')
        }

        // TODO: send updated playlist to all sockets
        const interval = setInterval(sendQueue, 5_000)

        timers.SetByIdAsync(userId, interval)
        sendQueue()
      } catch (error) {
        console.error('room:creation:request', error)
      }
    })

    socket.on('room:tracks:add', async (trackUris: string[]) => {
      const user = await users.GetByIdAsync(socket.id)

      if (!user?.roomId) return // TODO error

      const room = await Room.GetRoom(user.roomId)
      const tracksAdded = await room.AddTracks(trackUris)

      if (tracksAdded <= 0) return

      await room.SaveRoom()

      io.to(room.id).emit('room:queue', room.queue)
    })

    // socket.on('add:track', (pin: string, trackId: string) => {
    //   // TODO: validate if trackId exists in spotify
    //   roomStore.Modify(pin, room => {
    //     room.AddTrackRequest(trackId)
    //     io.to(room.pin).emit('track:request:added', trackId)
    //   })
    // })

    // socket.on('active:track', (pin: string, trackRequestId: string) => {
    //   roomStore.Modify(pin, room => {
    //     room.SetActiveTrackRequest(trackRequestId)
    //     io.to(room.pin).emit('track:active', trackRequestId)
    //   })
    // })

    // socket.on('remove:track', (pin: string, trackRequestId: string) => {
    //   roomStore.Modify(pin, room => {
    //     room.RemoveTrackRequest(trackRequestId)
    //     io.to(room.pin).emit('track:request:removed', trackRequestId)
    //   })
    // })

    // /**
    //  * Voting
    //  */
    // socket.on('vote:track', (pin: string, vote: Vote) => {
    //   roomStore.Modify(pin, room => {
    //     room.VoteTrackRequest(vote)
    //     io.to(room.pin).emit('track:request:voted', room.votes)
    //   })
    // })

    //#region Spotify authorization
    // TODO update token if socket is room owner
    socket.on('auth:token:spotify', async (accessToken: string) => {
      console.log('setting spotify token for', socket.id)
      try {
        const user = await getUserDataAsync(accessToken)

        users.SetByIdAsync(socket.id, {
          token: accessToken,
          userId: user.id,
        })

        // const testRoom = new Room()
        // testRoom.CreateRoom('AAAA', user.id, accessToken)
        // testRoom.SetPlaylistId('4GiturjKc5NOWR4eu0cYW3')
        // testRoom.Play()
        // rooms.SetByIdAsync(testRoom.pin, { id: testRoom.id, host: user.id })
        // testRoom.SaveRoom()
      } catch {
        console.error('Error while getting user data with access token')
      }
    })

    socket.on('auth:request:spotify', async (code: string) => {
      try {
        const token = await authorizationCodeGrantAsync(
          code,
          socket.request.headers.origin
        )

        if (!token) return // TODO error handling

        const user = await getUserDataAsync(token.body.access_token)
        updateUser(socket.id, { token: token.body.access_token, userId: user.id })

        socket.emit('auth:response:spotify', token)
      } catch {
        console.error('Error while authenticating with Spotify')
      }
    })

    socket.on('auth:refresh:spotify', async (refreshToken: string) => {
      console.log('refreshing spotify token for', socket.id)
      try {
        const token = await refreshAccessTokenAsync(
          refreshToken,
          socket.request.headers.origin
        )

        if (!token) return // TODO error handling

        const user = await getUserDataAsync(token.body.access_token)
        updateUser(socket.id, { token: token.body.access_token, userId: user.id })

        socket.emit('auth:refreshed:spotify', token)
      } catch {
        console.error('Error while refreshing Spotify token')
      }
    })
    //#endregion Spotify authorization

    socket.on('disconnect', () => {
      console.log('disconnecting', socket.id)
      users.RemoveByIdAsync(socket.id)
    })
  })

  // io.on('connect', (socket: Socket) => {
  //   console.log(socket)
  // })

  // io.attach(server, {
  //   cookie: false,
  // })
}
