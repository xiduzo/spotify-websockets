import { v4 as uuid } from 'uuid'

export class Builder<T> {
  private static _id: string = uuid()

  protected _value: T & {
    id: string
  }

  constructor() {}

  /**
   * Returns `<T>` as object
   */
  Build(): T {
    return {
      id: Builder._id,
      ...this._value,
    }
  }
}
