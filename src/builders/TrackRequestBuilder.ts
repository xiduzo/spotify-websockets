import { TrackRequest } from 'src/lib/interface/TrackRequest'
import { Builder } from './Builder'

export class TrackRequestBuilder extends Builder<TrackRequest> {
  constructor() {
    super()
  }

  WithTrackId(trackId: string): TrackRequestBuilder {
    this._value.trackId = trackId
    return this
  }
}
