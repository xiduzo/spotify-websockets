FROM node:14.1-alpine AS builder

# ADD files
COPY tsconfig.json .
COPY package.json package-lock.json .

# Install dependencies
RUN npm ci

# Build the image
COPY src src
RUN npm run build

# Cleanup
RUN rm -rf src package.json package-lock.json

# Run server
CMD ["node", "build/index.js"]